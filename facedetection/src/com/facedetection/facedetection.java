package com.facedetection;
//programa de deteccion de caras
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

public class facedetection {

	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		//seleccion de archivo en este caso imagenes
		String imgFile = "image/caras.jpg";
		Mat src = Imgcodecs.imread(imgFile);
		
		String imgFile1 = "image/caras.jpg";
		Mat src1 = Imgcodecs.imread(imgFile1);
		
		//se selecciona el archivo xml con el que se usara para la deteccion 
		//deteccion mas no reconocimiento
		String xmlFile = "xml/haarcascade_frontalface_default.xml";
		CascadeClassifier cc = new CascadeClassifier(xmlFile);

		String xmlFile1 = "xml/haarcascade_eye.xml";
		CascadeClassifier cc1 = new CascadeClassifier(xmlFile1);

		
		MatOfRect faceDetection = new MatOfRect();
		cc.detectMultiScale(src, faceDetection);
		System.out.println(String.format("Detected faces: %d", faceDetection.toArray().length));
		//encierra en un cuadro el elemento detectado
		for(Rect rect: faceDetection.toArray()) {
			Imgproc.rectangle(src, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height) , new Scalar(0, 0, 250), 3);
		}
		
		MatOfRect eyeDetection = new MatOfRect();
		cc1.detectMultiScale(src1, eyeDetection);
		System.out.println(String.format("Detected eyes: %d", eyeDetection.toArray().length));
		//encierra en un cuadro el elemento detectado
		for(Rect rect: eyeDetection.toArray()) {
			Imgproc.rectangle(src1, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height) , new Scalar(0, 143, 47), 3);
		}
		
		//crea una copia del archivo con el resultado 
		Imgcodecs.imwrite("image/caras_out.jpg", src);
		Imgcodecs.imwrite("image/caras_out2.jpg", src1);
		System.out.println("Image Detection Finished");
	}
}